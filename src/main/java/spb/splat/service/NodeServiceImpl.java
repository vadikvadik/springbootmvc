package spb.splat.service;

import org.springframework.stereotype.Service;
import spb.splat.dao.NodeDAO;
import spb.splat.model.Node;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;

@Service
public class NodeServiceImpl implements NodeService
{
    @Autowired
    NodeDAO nodeDAO;


    @Override
    public List<Node> getChildNodes(final long id)
    {
        return nodeDAO.getChildNodes(id);
    }


    @Override
    public Node getRoot()
    {
        return nodeDAO.getRoot();
    }


    @Override
    public long addNode(final Node node)
    {
        return nodeDAO.addNode(node);
    }


    @Override
    public boolean deleteNodes(final int id)
    {
        return nodeDAO.deleteNodes(id);
    }


    @Override
    public boolean renameNode(final Node node)
    {
        return nodeDAO.renameNode(node);
    }


    @Override
    public boolean moveNode(final long id, final long parentId)
    {
        return nodeDAO.moveNode(id, parentId);
    }
}
