CREATE TABLE node
        (
        id bigserial NOT NULL,
        name character varying(255),
        parent_id bigint,
        CONSTRAINT pk_node PRIMARY KEY (id )
        );
INSERT INTO node (name, parent_id) VALUES ('root', 0);