$(document).ready(function() {

    var switcher = {};
    switcher.maroon = $('.root').attr("id");
    switchHandle(switcher);
    $(".dropdown-toggle-js").dropdown();


    $(document).on("submit", ".form-add", function (e) {
        $('#basicModal').modal('hide');
        var object = form_to_object($(this));
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/node",
            dataType: 'json',
            data: JSON.stringify(object),
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $('form[node-add-id=' + object.id + ']').parent().addClass("display-none");
                var parentNode = $("#" + object.id);

                if ($("#" + object.id).children(".angle").children(".fa-angle-down").length > 0) {
                    parentNode.children(".children").append(generateNode(data, object.name, object.parentId));
                    switchHandle(switcher);
                }
            }
        });
    });

    $(document).on("click", ".dropdown-menu", function (e) {
        $('input[name=id]').val($(e.currentTarget).attr("node-id"));
    });

    $(document).on("click", ".arrow", function (e) {
        var param = $(e.currentTarget).parent().attr("id");
        var selectedObject =  $("#" + param);
        selectedObject.children(".arrow").children(".fa").removeClass("fa-angle-right");
        selectedObject.children(".arrow").children(".fa").addClass("fa-cog fa-spin");
        selectedObject.children(".arrow").removeClass('arrow');
        $("#" + switcher.maroon).children(".dropdown").children('.switch').children(".node-name").removeClass('maroon');
        switcher.maroon = param;
        $("#" + switcher.maroon).children(".dropdown").children('.switch').children(".node-name").addClass('maroon');
        $.ajax({
            type: "GET",
            url: "/node",
            data:{id:param},
            success: function (data) {

                setTimeout(function () {
                    selectedObject.children(".folder").children(".fa").removeClass('fa-folder-o');
                    selectedObject.children(".folder").children(".fa").addClass('fa-folder-open-o');
                    selectedObject.children(".angle").children(".fa").removeClass('fa-cog');
                    selectedObject.children(".angle").children(".fa").removeClass('fa-spin');
                    selectedObject.children(".angle").children(".fa").addClass('fa-angle-down');

                    data.forEach(function(item, i, arr) {
                        selectedObject.children(".children").append(generateNode(item.id, item.name, item.parentId));
                    });

                    switchHandle(switcher);
                    selectedObject.children(".angle").addClass('arrow-down');

                }, 2000)
            },
            error: function () {
                selectedObject.children(".angle").addClass('arrow');
            }
        });

    });


    $(document).on("click", ".arrow-down", function (e) {
        var param = $(e.currentTarget).parent().attr("id");
        var selectedObject =  $("#" + param);

        $("#" + switcher.maroon).children(".dropdown").children('.switch').children(".node-name").removeClass('maroon');
        switcher.maroon = param;
        $("#" + switcher.maroon).children(".dropdown").children('.switch').children(".node-name").addClass('maroon');

        selectedObject.children(".angle").children(".fa").removeClass('fa-angle-down');
        selectedObject.children(".angle").children(".fa").addClass('fa-angle-right');
        selectedObject.children(".folder").children(".fa").removeClass('fa-folder-open-o');
        selectedObject.children(".folder").children(".fa").addClass('fa-folder-o');
        selectedObject.children(".children").children().remove();
        selectedObject.children(".angle").removeClass('arrow-down');
        selectedObject.children(".angle").addClass('arrow');
    });

    $(document).on("click", ".add-node", function (e) {
        var a = $(e.currentTarget).parent().attr("node-id");
        $('form[node-add-id=' + a + ']').parent().removeClass("display-none");
    });

    $(document).on("click", ".cancel-add", function (e) {
        var a = $(e.currentTarget).parent().attr("node-add-id");
        $('form[node-add-id=' + a + ']').parent().addClass("display-none");
    });


    $(document).on("click", ".rename-node", function (e) {
        var a = $(e.currentTarget).parent().attr("node-id");
        $('form[node-rename-id=' + a + ']').parent().removeClass("display-none");
    });

    $(document).on("click", ".cancel-rename", function (e) {
        var a = $(e.currentTarget).parent().attr("node-rename-id");
        $('form[node-rename-id=' + a + ']').parent().addClass("display-none");
    });

    $(document).on("click", ".delete-node", function (e) {
        var param = $(e.currentTarget).parent().attr("node-id");
        $.ajax({
            type: "POST",
            url: "/delete_node",
            data:{id:param},
            success: function (data) {
                if (data == true) {
                    $("#"+param).remove();
                }
                else {
                    alert('Error for process delete')
                }
            }
        });

    });


    $(document).on("submit", ".form-rename", function (e) {
        $('#basicRenameModal').modal('hide');
        var object = form_to_object($(this));

        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/rename_node",
            dataType: 'json',
            data: JSON.stringify(object),
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                if (data == true)
                {
                    $("#" + object.id).children(".dropdown").children(".dropdown-toggle").children(".node-name").text(object.name);
                    $("#" + object.id).children(".dropdown").children(".dropdown-menu").attr("node-name", object.name);
                    $('form[node-rename-id=' + object.id + ']').parent().addClass("display-none");
                    $('form[node-rename-id=' + object.id + ']').children("input").val(object.name);
                }else {
                    alert("Error");
                }
            }
        });
    });
});

    function form_to_object(selector) {
        var ary = $(selector).serializeArray();
        var obj = {};
        for (var a = 0; a < ary.length; a++) obj[ary[a].name] = ary[a].value;
        return obj;
    }

    function generateNode(id, name, parentId) {
        var result =
            '<li id=' + id + ' class="li-element">' +
            '<span class="arrow angle"><i class="fa fa-angle-right"></i></span>' +
            '<span class="folder"> <i class="fa fa-folder-o"></i></span>'+
            '<div class="dropdown">' +
            '<span class="switch dropdown-toggle" data-toggle="dropdown" >' +
             '<span class="node-name">' + name + '</span>' +
            '<span class="caret"></span> </span>'+
            '<ul class = "dropdown-menu" ' + 'node-id=' + '"' + id + '"' + ' node-name=' + '"' + name + '"' + ' node-parentId = ' + '"' + parentId + '"' + '>' +
            '<li class="add-node"> <a href = "#"> Add </a> </li>' +
            '<li class="rename-node"> <a href = "#"> Rename </a> </li>' +
            '<li class="delete-node"> <a href = "#"> Delete </a> </li>' +
            '</ul>' +
            '</div>' +
            '<ul class="children" >' +
            '</ul>' +
            '</li>' +
            '<div class="display-none">' +
            '<form class="form-add" node-add-id="' + id + '">' +
            '<input name="name" type="text" class="form-control" required value=""/>' +
            '<input name="id" type="hidden" value ="' + id +'" />' +
            '<button type="button" class="btn btn-default cancel-add">Close</button>' +
            '<button type="submit" class="btn btn-primary">Add node</button>' +
        '</form>' +
        '</div>' +
        '<div class="display-none">' +
            '<form class="form-rename" node-rename-id="' + id +'">' +
            '<input name="name" class="form-control" type="text" value="'+ name +'" required/>' +
            '<input name="id" type="hidden" value ="" />' +
            '<button type="button" class="btn btn-default cancel-rename">Close</button>' +
            '<button type="submit" class="btn btn-primary">Rename node</button>' +
        '</form>' +
        '</div>';

        return result;
    }

    function switchHandle(switcher) {
        $(".switch").click(function (e) {
            $("#" + switcher.maroon).children(".dropdown").children('.switch').children(".node-name").removeClass('maroon');
            switcher.maroon = $(e.currentTarget).parent().children('ul').attr("node-id");
            $("#" + switcher.maroon).children(".dropdown").children('.switch').children(".node-name").addClass('maroon');
        });
    }